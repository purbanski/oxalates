//
//  ViewController.h
//  SQLite3DBSample
//
//  Created by Gabriel Theodoropoulos on 25/6/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EditInfoViewController.h"
#import "SettingsTableViewController.h"
#import "OxalateTableCell.h"
@interface ViewController : UIViewController <
    UITableViewDelegate,
    UITableViewDataSource,
    EditInfoViewControllerDelegate,
    SettingsTableControllerDelegate,
    OxalateTableCellDelegate,
    UISearchBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tblFood;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBarFood;
@property (weak, nonatomic) IBOutlet UIView *settingsView;
//@property (weak, nonatomic) IBOutlet UIBarButtonItem *settingsBtn;


- (IBAction)addNewRecord:(id)sender;
//- (IBAction)showSearchSettings:(id)sender;
//- (IBAction)btnSettings:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnSettings;
@property (weak, nonatomic) IBOutlet UIButton *btnSettingsBack;

- (IBAction)btnSettingsTouch:(id)sender;


@end
