//
//  EditInfoViewController.m
//  SQLite3DBSample
//
//  Created by Gabriel Theodoropoulos on 25/6/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "EditInfoViewController.h"
#import "DBManager.h"
#import "DBOxalate.h"
#import "Config.h"

@interface EditInfoViewController ()
{
    NSMutableArray *_pickerData;
}
@property NSInteger oxalateGroup;
@property BOOL dirty;

//@property (nonatomic, strong) DBManager *dbManager;

-(void)loadInfoToEdit;

@end


@implementation EditInfoViewController
//@synthesize oxalateGroup;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.recordIDToEdit = -1;
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    // Initialize Data
//    _pickerData = @[ @[@">10mg", @"2-10mg", @"<2mg"],
//                     @[@"napoje", @"owoce", @"warzywa",
//                       @"mleczne", @"skrobia", @"tłuszcz, orzechy",
//                       @"przyprawy", @"mięso", @"inne"]];    // Connect data
//    
//    _pickerData = @[@"napoje", @"owoce", @"warzywa",
//                    @"mleczne", @"skrobia", @"tłuszcz, orzechy",
//                    @"przyprawy", @"mięso", @"inne"];
    
    NSArray *tmp = [[DBOxalate shared] getFoodGroup];
    _pickerData = [NSMutableArray arrayWithCapacity:32];

    for (id obj in tmp )
        [_pickerData addObject:obj[1]];

    
    self.foodGroupPicker.dataSource = self;
    self.foodGroupPicker.delegate = self;
    
    // Make self the delegate of the textfields.
    self.txtFoodName.delegate = self;

    // Set the navigation bar tint color.
    self.navigationController.navigationBar.tintColor = self.navigationItem.rightBarButtonItem.tintColor;
    
    // Initialize the dbManager object.
//    self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"oxalates.db"];
    
    // Check if should load specific record for editing.
    if (self.recordIDToEdit != -1) {
        // Load the record with the specific ID from the database.
        [self loadInfoToEdit];
    }
//    self.oxalateGroupButton = 0;
    [self btnOxalateValueChanged:self];
}

-(void)viewWillAppear:(BOOL)animated
{
    self.lblOxLowDesc.text      = @"(mniejsza niż 2mg w 100g)";
    self.lblOxMediumDesc.text   = @"(w przedziale od 2 do 10mg w 100g)";
    self.lblOxHighDesc.text     = @"(większa niż 10mg w 100g)";

    self.dirty = FALSE;
    [self btnSaveUpdate];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UIPickerViewDelegate method
//- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//
//    NSDictionary *attrsDictionary = @{
//                                      NSFontAttributeName:[UIFont fontWithName:@"HelveticaNeue" size:64.0],
//                                      NSForegroundColorAttributeName: [UIColor whiteColor],
////                                      NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)
//                                      };
//    
//    NSAttributedString *attString = [[NSAttributedString alloc] initWithString:_pickerData[row]
//                                                                    attributes:attrsDictionary];
//    
//    return attString;
//}

// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _pickerData.count;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _pickerData[row];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.dirty = TRUE;
    [self btnSaveUpdate];
}

#pragma mark - UITextFieldDelegate method implementation

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - IBAction method implementation

- (IBAction)saveInfo:(id)sender {
    NSString *foodName;
    NSInteger foodId, foodGroup;
    NSInteger ret;
    
    foodName    = self.txtFoodName.text;
    foodGroup   = [self.foodGroupPicker selectedRowInComponent:0];
    foodId      = self.recordIDToEdit;
    
    if ( foodId != -1 )
        ret = [[DBOxalate shared] updateFood:foodName
                                      foodId:foodId
                                   foodGroup:foodGroup
                                oxalateGroup:self.oxalateGroup];
    else
        ret = [[DBOxalate shared] insertFood:foodName
                                   foodGroup:foodGroup
                                oxalateGroup:self.oxalateGroup];
    
    
    if ( ! ret )
    {
        // Inform the delegate that the editing was finished.
        [self.delegate editingInfoWasFinished];
        
        // Pop the view controller.
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)btnOxalateValueChanged:(id)sender {
//      int oxalateGroup = [self.oxalateGroupButton selectedSegmentIndex];
    CGFloat dur;
    dur = 0.5;
    self.dirty = TRUE;
    
    switch (self.oxalateGroup) {
        case 0:
            self.view.backgroundColor = [[Config shared] get][@"ColorBgGood"];
            
            [self fadeIn:self.lblOxLowDesc duration:dur];
            [self fadeOut:self.lblOxMediumDesc duration:dur];
            [self fadeOut:self.lblOxHighDesc duration:dur];

            break;
            
        case 1:
            self.view.backgroundColor = [[Config shared] get][@"ColorBgMedium"];

            [self fadeOut:self.lblOxLowDesc duration:dur];
            [self fadeIn:self.lblOxMediumDesc duration:dur];
            [self fadeOut:self.lblOxHighDesc duration:dur];
            
            break;
            
        case 2:
            self.view.backgroundColor = [[Config shared] get][@"ColorBgBad"];
            
            [self fadeOut:self.lblOxLowDesc duration:dur];
            [self fadeOut:self.lblOxMediumDesc duration:dur];
            [self fadeIn:self.lblOxHighDesc duration:dur];
            
            break;
            
        default:
            break;
    }
    
    self.btnViewPanel.backgroundColor = self.view.backgroundColor;
    [self btnSaveUpdate];
}

-(void) fadeOut:(UIView*)view duration:(CGFloat)duration
{
    if ([view alpha] != 0.0)
        [UIView animateWithDuration:duration animations:^{
            [view setAlpha:0.0];
        } completion:^(BOOL finished) {
        }];
}

-(void) fadeIn:(UIView*)view duration:(CGFloat)duration
{
    if ([view alpha] != 1.0)
        [UIView animateWithDuration:duration animations:^{
            [view setAlpha:1.0];
        } completion:^(BOOL finished) {
        }];
}

- (IBAction)btnOxHighTouch:(id)sender {
    self.oxalateGroup = 2;
    
    [self btnOxalateValueChanged:sender];
}

- (IBAction)btnOxMidTouch:(id)sender {
    self.oxalateGroup = 1;
    [self btnOxalateValueChanged:sender];
}

- (IBAction)btnOxLowTouch:(id)sender {
    self.oxalateGroup = 0;
    [self btnOxalateValueChanged:sender];
}

- (IBAction)textFieldProductNameValueChange:(id)sender {
    self.dirty = TRUE;
    [self btnSaveUpdate];
}

-(void) btnSaveUpdate {
    if ([self.txtFoodName.text length]>0 && self.dirty )
    {
        [self.btnSave setEnabled:TRUE];
    }
    else
        [self.btnSave setEnabled:FALSE];
}

#pragma mark - Private method implementation

-(void)loadInfoToEdit{
    NSArray *results;
    results = [[DBOxalate shared] getFood:self.recordIDToEdit];

    if ([results count ] == 0 )
        return;
    
    self.txtFoodName.text = results[0][1];
    [self.foodGroupPicker selectRow:[results[0][2] integerValue] inComponent:0 animated:true];
    
    self.oxalateGroup = [results[0][3] integerValue];
    NSLog(@"Group %d", [results[0][3]integerValue]);
//    [self.oxalateGroupButton setSelectedSegmentIndex:[results[0][3] integerValue]];
}



@end
