//
//  SettingsTableViewController.m
//  SQLite3DBSample
//
//  Created by Przemek Urbanski on 25/12/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "SettingsTableViewController.h"
#import "Settings.h"

@interface SettingsTableViewController ()

@end

@implementation SettingsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setSwitches];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return 11;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - private
-(void) setSwitches
{
    //    NSLog(@"Log : ----------------------------------------");
    
    [self setSwitch:self.swOxaLow       switchName:@"SwitchOxalateLow"];
    [self setSwitch:self.swOxaMedium    switchName:@"SwitchOxalateMedium"];
    [self setSwitch:self.swOxaHigh      switchName:@"SwitchOxalateHigh"];
    
    [self setSwitch:self.swFoodDrink    switchName:@"SwitchFoodDrink"];
    [self setSwitch:self.swFoodFruit    switchName:@"SwitchFoodFruit"];
    [self setSwitch:self.swFoodVegetable     switchName:@"SwitchFoodVegetable"];
    [self setSwitch:self.swFoodSugar    switchName:@"SwitchFoodSugar"];
    [self setSwitch:self.swFoodFat      switchName:@"SwitchFoodFat"];
    [self setSwitch:self.swFoodMilk     switchName:@"SwitchFoodMilk"];
    //    NSLog(@"Log : ----------------------------------------");
    
}

-(void) setSwitch:(UISwitch*)swi switchName:(NSString*)setName
{
    //    NSLog(@"Log : %@", [[Settings shared]get] [setName]);
    if ( [([[Settings shared]get] [setName]) integerValue] )
        [swi setOn:TRUE animated:TRUE];
    else
        [swi setOn:FALSE animated:TRUE];
}

-(void) setSettings:(UISwitch*)sw
            setName:(NSString*)setName
{
    if ( [sw isOn] )
        [[Settings shared]set:setName value:@YES];
    else
        [[Settings shared]set:setName value:@NO];
    
}

- (IBAction)switchedOxaLow:(id)sender {
    [self setSettings:self.swOxaLow setName:@"SwitchOxalateLow"];
    [self.delegate settingsChanged];
}

- (IBAction)switchedOxaMedium:(id)sender {
    [self setSettings:self.swOxaMedium setName:@"SwitchOxalateMedium"];
    [self.delegate settingsChanged];
}

- (IBAction)switchedOxaHigh:(id)sender {
    [self setSettings:self.swOxaHigh setName:@"SwitchOxalateHigh"];
    [self.delegate settingsChanged];
}

- (IBAction)switchedFoodDrink:(id)sender {
    [self setSettings:self.swFoodDrink setName:@"SwitchFoodDrink"];
    [self.delegate settingsChanged];
}

- (IBAction)switchedFoodFruit:(id)sender {
    [self setSettings:self.swFoodFruit setName:@"SwitchFoodFruit"];
    [self.delegate settingsChanged];
}

- (IBAction)switchedFoodVegetable:(id)sender{
    [self setSettings:self.swFoodVegetable setName:@"SwitchFoodVegetable"];
    [self.delegate settingsChanged];
}

- (IBAction)switchedFoodSugar:(id)sender {
    [self setSettings:self.swFoodSugar setName:@"SwitchFoodSugar"];
    [self.delegate settingsChanged];
}

- (IBAction)switchedFoodFat:(id)sender {
    [self setSettings:self.swFoodFat setName:@"SwitchFoodFat"];
    [self.delegate settingsChanged];
}

- (IBAction)switchedFoodMilk:(id)sender {
    [self setSettings:self.swFoodMilk setName:@"SwitchFoodMilk"];
    [self.delegate settingsChanged];
}
@end
