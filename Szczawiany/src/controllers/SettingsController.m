//
//  ViewController.m
//  SQLite3DBSample
//
//  Created by Gabriel Theodoropoulos on 25/6/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "SettingsController.h"
#import "DBOxalate.h"
#import "Config.h"
#import "Settings.h"

@interface SettingsController ()

@end

@implementation SettingsController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setSwitches];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - private
-(void) setSwitches
{
//    NSLog(@"Log : ----------------------------------------");

    [self setSwitch:self.swOxaLow       switchName:@"SwitchOxalateLow"];
    [self setSwitch:self.swOxaMedium    switchName:@"SwitchOxalateMedium"];
    [self setSwitch:self.swOxaHigh      switchName:@"SwitchOxalateHigh"];
    
    [self setSwitch:self.swFoodDrink    switchName:@"SwitchFoodDrink"];
    [self setSwitch:self.swFoodFruit    switchName:@"SwitchFoodFruit"];
    [self setSwitch:self.swFoodVegetable     switchName:@"SwitchFoodVegetable"];
    [self setSwitch:self.swFoodSugar    switchName:@"SwitchFoodSugar"];
    [self setSwitch:self.swFoodFat      switchName:@"SwitchFoodFat"];
    [self setSwitch:self.swFoodMilk     switchName:@"SwitchFoodMilk"];
//    NSLog(@"Log : ----------------------------------------");

}

-(void) setSwitch:(UISwitch*)swi switchName:(NSString*)setName
{
//    NSLog(@"Log : %@", [[Settings shared]get] [setName]);
    if ( [([[Settings shared]get] [setName]) integerValue] )
        [swi setOn:TRUE animated:TRUE];
    else
        [swi setOn:FALSE animated:TRUE];
}

-(void) setSettings:(UISwitch*)sw
            setName:(NSString*)setName
{
    if ( [sw isOn] )
        [[Settings shared]set:setName value:@YES];
    else
        [[Settings shared]set:setName value:@NO];
    
}

- (IBAction)switchedOxaLow:(id)sender {
    [self setSettings:self.swOxaLow setName:@"SwitchOxalateLow"];
    [self.delegate settingsChanged];
}

- (IBAction)switchedOxaMedium:(id)sender {
    [self setSettings:self.swOxaMedium setName:@"SwitchOxalateMedium"];
    [self.delegate settingsChanged];
}

- (IBAction)switchedOxaHigh:(id)sender {
    [self setSettings:self.swOxaHigh setName:@"SwitchOxalateHigh"];
    [self.delegate settingsChanged];
}

- (IBAction)switchedFoodDrink:(id)sender {
    [self setSettings:self.swFoodDrink setName:@"SwitchFoodDrink"];
    [self.delegate settingsChanged];
}

- (IBAction)switchedFoodFruit:(id)sender {
    [self setSettings:self.swFoodFruit setName:@"SwitchFoodFruit"];
    [self.delegate settingsChanged];
}

- (IBAction)switchedFoodVegetable:(id)sender{
    [self setSettings:self.swFoodVegetable setName:@"SwitchFoodVegetable"];
    [self.delegate settingsChanged];
}

- (IBAction)switchedFoodSugar:(id)sender {
    [self setSettings:self.swFoodSugar setName:@"SwitchFoodSugar"];
    [self.delegate settingsChanged];
}

- (IBAction)switchedFoodFat:(id)sender {
    [self setSettings:self.swFoodFat setName:@"SwitchFoodFat"];
    [self.delegate settingsChanged];
}

- (IBAction)switchedFoodMilk:(id)sender {
    [self setSettings:self.swFoodMilk setName:@"SwitchFoodMilk"];
    [self.delegate settingsChanged];
}

@end
