//
//  OxalateTableCell.h
//  Szczawiany
//
//  Created by Przemek Urbanski on 29/01/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol OxalateTableCellDelegate

-(void) oxCellBtnEditTouched:(NSInteger)recordId;

@end

@interface OxalateTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UILabel *lblProductName;
@property (strong, nonatomic) id<OxalateTableCellDelegate> delegate;

@property NSInteger recordEditId;

- (IBAction)btnEditTouched:(id)sender;
@end
