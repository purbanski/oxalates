//
//  ViewController.h
//  SQLite3DBSample
//
//  Created by Gabriel Theodoropoulos on 25/6/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SettingsControllerDelegate

-(void)settingsChanged;

@end


@interface SettingsController : UIViewController

@property (weak, nonatomic) IBOutlet UINavigationBar *navBar;

@property (nonatomic, strong) id<SettingsControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UISwitch *swOxaLow;
@property (weak, nonatomic) IBOutlet UISwitch *swOxaMedium;
@property (weak, nonatomic) IBOutlet UISwitch *swOxaHigh;

@property (weak, nonatomic) IBOutlet UISwitch *swFoodDrink;
@property (weak, nonatomic) IBOutlet UISwitch *swFoodFruit;
@property (weak, nonatomic) IBOutlet UISwitch *swFoodVegetable;
@property (weak, nonatomic) IBOutlet UISwitch *swFoodSugar;
@property (weak, nonatomic) IBOutlet UISwitch *swFoodFat;
@property (weak, nonatomic) IBOutlet UISwitch *swFoodMilk;

- (IBAction)switchedOxaLow:(id)sender;
- (IBAction)switchedOxaMedium:(id)sender;
- (IBAction)switchedOxaHigh:(id)sender;

- (IBAction)switchedFoodDrink:(id)sender;
- (IBAction)switchedFoodFruit:(id)sender;
- (IBAction)switchedFoodVegetable:(id)sender;

- (IBAction)switchedFoodSugar:(id)sender;
- (IBAction)switchedFoodFat:(id)sender;
- (IBAction)switchedFoodMilk:(id)sender;


@end
