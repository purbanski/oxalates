//
//  TestViewController.m
//  Szczawiany
//
//  Created by Przemek Urbanski on 30/01/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import "InfoController.h"
#import "Config.h"

static const float portaitContentHeight = 1277.0;
static float landscapeContentHeight;

@interface InfoController ()

@property CGFloat contentHeight;

@end

@implementation InfoController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    CGFloat screenHeight = screenRect.size.height;
    landscapeContentHeight = portaitContentHeight * screenWidth/screenHeight * 1.35;
    if (landscapeContentHeight > portaitContentHeight)
        landscapeContentHeight = portaitContentHeight;
    
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    self.contentHeight = portaitContentHeight;
    self.navigationController.navigationBar.tintColor = [[Config shared]get][@"ColorTint"];
    
//    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
//    
//    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
//    {
//        self.contentHeight = landscapeContentHeight;
//    }
//    else
//    {
//        self.contentHeight = portaitContentHeight;
//    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



#pragma mark - UITableView method implementation

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    cell =  [tableView dequeueReusableCellWithIdentifier:@"idInfoCell" forIndexPath:indexPath];
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return self.contentHeight;
}

#pragma mark - Orientation
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                               duration:(NSTimeInterval)duration
{

    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        self.contentHeight = landscapeContentHeight;
    }
    else
    {
        self.contentHeight = portaitContentHeight;
    }
}

@end
