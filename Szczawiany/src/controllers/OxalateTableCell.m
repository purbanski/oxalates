//
//  OxalateTableCell.m
//  Szczawiany
//
//  Created by Przemek Urbanski on 29/01/15.
//  Copyright (c) 2015 Appcoda. All rights reserved.
//

#import "OxalateTableCell.h"

@implementation OxalateTableCell

- (void)awakeFromNib {
    // Initialization code
    self.delegate = nil;
    self.recordEditId = -1;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnEditTouched:(id)sender {
    if (self.delegate)
    {
    
        [self.delegate oxCellBtnEditTouched:self.recordEditId];
    }
}
@end
