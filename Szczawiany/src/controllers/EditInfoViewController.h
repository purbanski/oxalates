//
//  EditInfoViewController.h
//  SQLite3DBSample
//
//  Created by Gabriel Theodoropoulos on 25/6/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol EditInfoViewControllerDelegate

-(void)editingInfoWasFinished;

@end


@interface EditInfoViewController : UIViewController <UITextFieldDelegate,UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) id<EditInfoViewControllerDelegate> delegate;

@property (weak, nonatomic) IBOutlet UITextField *txtFoodName;

@property (nonatomic) int recordIDToEdit;

@property (weak, nonatomic) IBOutlet UIPickerView *foodGroupPicker;
//@property (weak, nonatomic) IBOutlet UISegmentedControl *oxalateGroupButton;
@property (strong, nonatomic) IBOutlet UIView *viewMain;
@property (weak, nonatomic) IBOutlet UILabel *lblOxHighDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblOxMediumDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblOxLowDesc;
@property (weak, nonatomic) IBOutlet UIView *btnViewPanel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *btnSave;

- (IBAction)saveInfo:(id)sender;
- (IBAction)btnOxalateValueChanged:(id)sender;

- (IBAction)btnOxHighTouch:(id)sender;
- (IBAction)btnOxMidTouch:(id)sender;
- (IBAction)btnOxLowTouch:(id)sender;
- (IBAction)textFieldProductNameValueChange:(id)sender;

@end
