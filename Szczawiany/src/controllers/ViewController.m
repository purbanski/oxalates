#import "ViewController.h"
#import "DBOxalate.h"
#import "Config.h"
#import "SettingsTableViewController.h"
#import "OxalateTableCell.h"

@interface ViewController ()

@property (nonatomic, strong) NSArray *arrFood;
@property (nonatomic) int recordIDToEdit;
@property BOOL settingsShown;

-(void)loadData;

@end


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tblFood.delegate = self;
    self.tblFood.dataSource = self;
    self.settingsShown = FALSE;
    
    self.searchBarFood.delegate = self;

    [self loadData];
}

-(void)viewDidLayoutSubviews{
    [super viewDidLayoutSubviews];

    if (self.settingsShown)
    {
        [self.btnSettings setAlpha:0.0f];
        [self.btnSettingsBack setAlpha:1.0f];
        
        CGRect btFrame = self.btnSettings.frame;
        btFrame = self.btnSettingsBack.frame;
        btFrame.origin.x -= 160; ;
        btFrame.origin.y += 0;
        self.btnSettingsBack.frame = btFrame;
        self.btnSettingsBack.alpha = 1.0f;       
    }
    else
    {
        [self.btnSettings setAlpha:1.0f];
        [self.btnSettingsBack setAlpha:0.0f];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ( [[segue identifier] isEqualToString:@"idSegueEditInfo" ] )
    {
        EditInfoViewController *editInfoViewController = [segue destinationViewController];
        editInfoViewController.delegate = self;
        editInfoViewController.recordIDToEdit = self.recordIDToEdit;
    }

    if ( [[segue identifier] isEqualToString:@"idSegueSetSettings" ] )
    {
        SettingsTableViewController *viewController = [segue destinationViewController];
        viewController.delegate = self;
    }
}


#pragma mark - IBAction method implementation

- (IBAction)addNewRecord:(id)sender {
    // Before performing the segue, set the -1 value to the recordIDToEdit. That way we'll indicate that we want to add a new record and not to edit an existing one.
    self.recordIDToEdit = -1;
    
    // Perform the segue.
    [self performSegueWithIdentifier:@"idSegueEditInfo" sender:self];
}

- (IBAction)btnSettings:(id)sender{
//    static BOOL sw = false;
    static float moveLength = 158.0f;
    
    CGRect rectHide = CGRectMake(320, 108, 300, 374);
    CGRect rectShow = CGRectMake(100, 108, 300, 374);
    CGRect frame = self.settingsView.frame;
    
    rectShow = frame;
    rectHide = frame;
    
    rectShow.origin.x = rectShow.origin.x - 0;
    rectHide.origin.x = rectHide.origin.x + moveLength;

    self.settingsShown = !self.settingsShown;
    if ( self.settingsShown )
    {
        /* Show */
        [self.settingsView setHidden:NO];
//        [self.settingsBtn setTitle:@"Schowaj"];

        self.settingsView.alpha = 1.0f;
        self.settingsView.frame =  rectHide;
        [UIView animateWithDuration:0.25 animations:^{
            self.settingsView.frame =  rectShow;
//            self.settingsView.alpha = 1.0f;
        
            CGRect btFrame = self.btnSettings.frame;
            btFrame.origin.x -= moveLength; ;
            btFrame.origin.y += 0;
            self.btnSettings.frame = btFrame;
            self.btnSettings.alpha = 0.0f;
   
            btFrame = self.btnSettingsBack.frame;
            btFrame.origin.x -= moveLength; ;
            btFrame.origin.y += 0;
            self.btnSettingsBack.frame = btFrame;
            self.btnSettingsBack.alpha = 1.0f;

        } completion:^(BOOL finished) {
        }];
    }
    else
    {
        /* Hide */
//        [self.settingsBtn setTitle:@"Ustawienia"];

        self.settingsView.frame =  rectShow;
        [UIView animateWithDuration:0.25 animations:^{
            self.settingsView.frame =  rectHide;
//            self.settingsView.alpha = 0.0f;

            CGRect btFrame = self.btnSettings.frame;
            btFrame.origin.x += moveLength; ;
            btFrame.origin.y += 0;
            self.btnSettings.frame = btFrame;
            self.btnSettings.alpha = 1.0f;

            btFrame = self.btnSettingsBack.frame;
            btFrame.origin.x += moveLength; ;
            btFrame.origin.y += 0;
            self.btnSettingsBack.frame = btFrame;
            self.btnSettingsBack.alpha = 0.0f;

        } completion:^(BOOL finished) {
            self.settingsView.alpha = 0.0f;
            self.settingsView.frame =  rectShow;
        }];
        
    }
}

- (IBAction)btnSettingsTouch:(id)sender {
    return [self btnSettings:sender];
}


#pragma mark - Private method implementation

-(void)loadData
{
    self.arrFood = [[DBOxalate shared] getFoodListWithSettings];
    [self.tblFood reloadData];
}


#pragma mark - UITableView method implementation

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.arrFood.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OxalateTableCell *cell;
    NSString *text, *dtext;
    NSInteger ind;
    NSArray *food;
    
    cell = (OxalateTableCell*) [tableView dequeueReusableCellWithIdentifier:@"idCellRecord" forIndexPath:indexPath];
    
//    cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"idCellRecord"] ;
//    cell ind
    ind = indexPath.row;
    food = self.arrFood[ind];
    
    text = [NSString stringWithFormat:@"%@", food[1] ];
//    cell.textLabel.text = text;

    dtext = [NSString stringWithFormat:@"%@ \t\t %@", food[3], food[5]];
//    cell.detailTextLabel.text = @"";
    
    cell.lblProductName.text = text;
    cell.textLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
    cell.detailTextLabel.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0];
//    cell.backgroundColor = [UIColor colorWithRed:1 green:0 blue:0 alpha:1];
    cell.delegate = self;
    cell.recordEditId = [food[0] integerValue];
    
    switch ( [food[4] integerValue] ) {
        case 0:
            cell.backgroundColor = [[Config shared] get][@"ColorRecordGood-2"];
//            cell.textLabel.textColor = [[Config shared]get][@"ColorRecordGood"];
            break;
            
        case 1:
            cell.backgroundColor = [[Config shared] get][@"ColorRecordMedium-2"];
//            cell.textLabel.textColor = [[Config shared]get][@"ColorRecordMedium"];
            
            break;
            
        case 2:
            cell.backgroundColor = [[Config shared] get][@"ColorRecordBad-2"];
//            cell.textLabel.textColor = [[Config shared]get][@"ColorRecordMedium"];
            break;
            
        default:
            break;
    }
    
    return cell;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50.0;
}


-(void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath{
    // Get the record ID of the selected name and set it to the recordIDToEdit property.
//    self.recordIDToEdit = [[[self.arrFood objectAtIndex:indexPath.row] objectAtIndex:0] intValue];
    
    NSInteger ind;
    NSArray *food;
    
    ind = indexPath.row;
    food = self.arrFood[ind];
    
    self.recordIDToEdit = [food[0] integerValue];
    
    NSLog(@"%d", indexPath.row);
    NSLog(@"%@", food);
    // Perform the segue.
    [self performSegueWithIdentifier:@"idSegueEditInfo" sender:self];
}


- (void) tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        int recordIDToDelete;
        
        recordIDToDelete = [[[self.arrFood objectAtIndex:indexPath.row] objectAtIndex:0] intValue];
        [[DBOxalate shared] deleteFood:recordIDToDelete];
        [self loadData];
    }
}

#pragma mark - OxalateTableCell delegate
-(void) oxCellBtnEditTouched:(NSInteger)recordId {

    self.recordIDToEdit = recordId;
    [self performSegueWithIdentifier:@"idSegueEditInfo" sender:self];
}

#pragma mark - EditInfoViewControllerDelegate method implementation

-(void)editingInfoWasFinished{
    // Reload the data.
    [self loadData];
}

#pragma mark - SettingsControllerDelegate method implementation
-(void) settingsChanged
{
    [self loadData];
}

#pragma mark - Search Bar Food
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
   return YES;
}

// called when text starts editing
//- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar;

// return NO to not resign first responder
//- (BOOL)searchBarShouldEndEditing:(UISearchBar *)searchBar
//{
//    return YES;
//}

// called when text ends editing
//- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar;

// called when text changes (including clear)
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    NSLog(@"search: %@", searchText);
    self.arrFood = [[DBOxalate shared] getFoodListLikeWithSettings:searchText];
    [self.tblFood reloadData];

}

// called before text changes
//- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text NS_AVAILABLE_IOS(3_0);

// called when keyboard search button pressed
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.searchBarFood performSelector:@selector(resignFirstResponder)
                             withObject:nil
                             afterDelay:0];
}

// called when bookmark button pressed
//- (void)searchBarBookmarkButtonClicked:(UISearchBar *)searchBar;

// called when cancel button pressed
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.arrFood = [[DBOxalate shared] getFoodListWithSettings];
    [self.tblFood reloadData];
    [self.searchBarFood setText:@""];
    [self.searchBarFood performSelector:@selector(resignFirstResponder)
                             withObject:nil
                             afterDelay:0];

}

// called when search results button pressed
//- (void)searchBarResultsListButtonClicked:(UISearchBar *)searchBar NS_AVAILABLE_IOS(3_2);

//- (void)searchBar:(UISearchBar *)searchBar selectedScopeButtonIndexDidChange:(NSInteger)selectedScope NS_AVAILABLE_IOS(3_0);

@end
