//
//  DBOxalate.h
//
//
//  Created by Przemek Urbanski on 23/12/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBOxalate : NSObject

+ (id)shared;
- (NSArray *) getFoodList;
- (NSArray *) getFoodListWithSettings;
- (NSArray *) getFoodListLike:(NSString*)pattern;
- (NSArray *) getFoodListLikeWithSettings:(NSString*)pattern;
- (NSArray *) getFood:(NSInteger) id;

- (NSArray *) getFoodGroup;
- (NSArray *) getOxalateGroup;

- (NSInteger) updateFood:(NSString*) foodName
                  foodId:(NSInteger)foodId
               foodGroup:(NSInteger)foodGroup
            oxalateGroup:(NSInteger)oxalateGroup;

- (NSInteger) insertFood:(NSString*) foodName
               foodGroup:(NSInteger)foodGroup
            oxalateGroup:(NSInteger)oxalateGroup;

- (void) deleteFood:(NSInteger)foodId;

@end

