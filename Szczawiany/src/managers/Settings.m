//
//  Settings.m
//  SQLite3DBSample
//
//  Created by Przemek Urbanski on 24/12/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "Settings.h"

@interface Settings()

@property (nonatomic, strong) NSMutableDictionary *conf;

@end

@implementation Settings

static Settings *sharedInstance = nil;

+ (id)shared {
    if (sharedInstance == nil ) {
        sharedInstance = [[ super allocWithZone:NULL] init];
    }
    return sharedInstance;
}

- (id)init {
    if (self = [super init])
    {
        self.conf = [[NSMutableDictionary alloc] init];
  
        self.conf[@"SwitchOxalateLow"]    = @YES;
        self.conf[@"SwitchOxalateMedium"] = @YES;
        self.conf[@"SwitchOxalateHigh"]   = @YES;

        self.conf[@"SwitchFoodDrink"]       = @YES;
        self.conf[@"SwitchFoodFruit"]       = @YES;
        self.conf[@"SwitchFoodVegetable"]   = @YES;
        self.conf[@"SwitchFoodSugar"]       = @YES;
        self.conf[@"SwitchFoodFat"]         = @YES;
        self.conf[@"SwitchFoodMilk"]        = @YES;

    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

-(void)dealloc {
}

#pragma mark - Getters

-(NSDictionary*) get
{
    return self.conf;
}

-(void) set:(NSString*)key value:(id)obj
{
    self.conf[key] = obj;
}


@end
