//
//  Settings.m
//  SQLite3DBSample
//
//  Created by Przemek Urbanski on 24/12/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "Config.h"
@interface Config()

@property (nonatomic, strong) NSMutableDictionary *conf;

@end

@implementation Config

static Config *sharedInstance = nil;

+ (id)shared {
    if (sharedInstance == nil ) {
        sharedInstance = [[ super allocWithZone:NULL] init];
    }
    return sharedInstance;
}

- (id)init {
    if (self = [super init])
    {

        self.conf = [[NSMutableDictionary alloc] init];
        float a = 1.0f;
        
        self.conf[@"ColorTint"]   = [UIColor colorWithRed:255.0/255.0f green:128.0/255.0 blue:0.0/255.0f alpha:a];

        self.conf[@"ColorRecordBad"]    = [UIColor colorWithRed:215.0f/255.0f green:67.0f/255.0 blue:46.0/255.0f alpha:a];
        self.conf[@"ColorRecordMedium"] = [UIColor colorWithRed:23.0f/255.0 green:107.0/255.0 blue:237.0/255.0 alpha:a];
        self.conf[@"ColorRecordGood"]   = [UIColor colorWithRed:88.0f/255.0f green:185.0/255.0 blue:71.0/255.0f alpha:a];

        a = 0.25f;
        self.conf[@"ColorRecordBad-2"]    = [UIColor colorWithRed:215.0f/255.0f green:67.0f/255.0 blue:46.0/255.0f alpha:a];
        self.conf[@"ColorRecordMedium-2"] = [UIColor colorWithRed:23.0f/255.0 green:107.0/255.0 blue:237.0/255.0 alpha:a];
        self.conf[@"ColorRecordGood-2"]   = [UIColor colorWithRed:88.0f/255.0f green:185.0/255.0 blue:71.0/255.0f alpha:a];
        
        a = 1.0f;
        self.conf[@"ColorBgBad"]    = [UIColor colorWithRed:245.0f/255.0f green:208.0f/255.0 blue:203.0/255.0f alpha:a];
        self.conf[@"ColorBgMedium"] = [UIColor colorWithRed:197.0/255.0 green:218.0/255.0 blue:250.0/255.0 alpha:a];
        self.conf[@"ColorBgGood"]   = [UIColor colorWithRed:213.0/255.0f green:237.0/255.0 blue:209.0/255.0f alpha:a];

    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

-(void)dealloc {
}

-(NSDictionary*) get
{
    return self.conf;
}
#pragma mark - Getters

@end
