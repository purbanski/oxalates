//
//  Settings.h
//  SQLite3DBSample
//
//  Created by Przemek Urbanski on 24/12/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Config : NSObject

+ (id)shared;

-(NSDictionary*) get;

@end
