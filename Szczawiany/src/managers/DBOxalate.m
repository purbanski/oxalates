//
//  DBOxalate.m
//  SQLite3DBSample
//
//  Created by Przemek Urbanski on 23/12/14.
//  Copyright (c) 2014 Appcoda. All rights reserved.
//

#import "DBOxalate.h"
#import "DBManager.h"
#import "Settings.h"

@interface DBOxalate ()

@property (nonatomic, strong) DBManager *dbManager;
//@property (nonatomic, strong) NSArray *arrFood;

@end

@implementation DBOxalate

static DBOxalate *sharedInstance = nil;

+ (id)shared {
    if (sharedInstance == nil ) {
        sharedInstance = [[ super allocWithZone:NULL] init];
    }
    return sharedInstance;
}

- (id)init {
    if (self = [super init]) {
        self.dbManager = [[DBManager alloc] initWithDatabaseFilename:@"oxalates.db"];
    }
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    return self;
}

-(void)dealloc {
}

#pragma mark - Queries
- (NSArray *) getFoodList
{
    return [self getFoodListWithSettings];
    
    NSString *query = @"select * from food";
    query = @"  SELECT food.id, food.name, foodGroup.id, foodGroup.name, oxalateGroup.id, oxalateGroup.name \
                FROM food \
                JOIN foodGroup ON food.foodGroup=foodGroup.id \
                JOIN oxalateGroup ON food.oxalateGroup=oxalateGroup.id \
                ORDER BY food.name; ";
    
    return [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
}

- (NSArray *) getFoodListWithSettings
{
    NSString *query = @"select * from food";
    query = [NSString stringWithFormat:@"  SELECT food.id, food.name, foodGroup.id, foodGroup.name, oxalateGroup.id, oxalateGroup.name \
    FROM food \
    JOIN foodGroup ON food.foodGroup=foodGroup.id \
    JOIN oxalateGroup ON food.oxalateGroup=oxalateGroup.id \
    %@ \
    ORDER BY food.name; ", [self getQueryWhere]];
    
//    NSLog(@"%@", query );

    return [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
}

-(NSString*) getQueryWhere {
    NSString *whereFood;
    NSString *whereOxalate;
    
    whereFood = @"";
    whereOxalate = @"";
    
    if ( [[[Settings shared]get][@"SwitchOxalateLow"] integerValue] == 1 )
        whereOxalate = [NSString stringWithFormat:@"%@ food.oxalateGroup=0 OR", whereOxalate];

    if ( [[[Settings shared]get][@"SwitchOxalateMedium"] integerValue] == 1 )
        whereOxalate = [NSString stringWithFormat:@"%@ food.oxalateGroup=1 OR", whereOxalate];
    
    if ( [[[Settings shared]get][@"SwitchOxalateHigh"] integerValue] == 1 )
        whereOxalate = [NSString stringWithFormat:@"%@ food.oxalateGroup=2 OR", whereOxalate];
    
    if ( [whereOxalate length] > 0 ) {
        whereOxalate = [whereOxalate substringWithRange:NSMakeRange(0, [whereOxalate length]-2)];
        whereOxalate = [NSString stringWithFormat:@"(%@)", whereOxalate];
    }
    
    //-------------------
    // Products
    
    if ( [[[Settings shared]get][@"SwitchFoodDrink"] integerValue] == 1 )
        whereFood = [NSString stringWithFormat:@"%@ food.foodGroup=0 OR", whereFood];
    
    if ( [[[Settings shared]get][@"SwitchFoodFruit"] integerValue] == 1 )
        whereFood = [NSString stringWithFormat:@"%@ food.foodGroup=1 OR", whereFood];
    
    if ( [[[Settings shared]get][@"SwitchFoodVegetable"] integerValue] == 1 )
        whereFood = [NSString stringWithFormat:@"%@ food.foodGroup=2 OR", whereFood];
    
    if ( [[[Settings shared]get][@"SwitchFoodSugar"] integerValue] == 1 )
        whereFood = [NSString stringWithFormat:@"%@ food.foodGroup=3 OR", whereFood];
    
    if ( [[[Settings shared]get][@"SwitchFoodFat"] integerValue] == 1 )
        whereFood = [NSString stringWithFormat:@"%@ food.foodGroup=4 OR", whereFood];
    
    if ( [[[Settings shared]get][@"SwitchFoodMilk"] integerValue] == 1 )
        whereFood = [NSString stringWithFormat:@"%@ food.foodGroup=5 OR", whereFood];

    if ( [whereFood length] > 0 ) {
        whereFood = [whereFood substringWithRange:NSMakeRange(0, [whereFood length]-2)];
        whereFood = [NSString stringWithFormat:@"(%@)", whereFood];
    }

    NSString *where;
    where = @"";
    
    if ([whereFood length] && [whereOxalate length])
        where = [ NSString stringWithFormat:@" WHERE %@ AND %@", whereFood, whereOxalate ];
    else
        where = [ NSString stringWithFormat:@" WHERE food.id=-1"];
    
//    else if ( ![whereFood length] && ![whereOxalate length] )
//    {
//        
//    }
//    else
//        where = [ NSString stringWithFormat:@" WHERE %@ %@", whereFood, whereOxalate ];
//    
    return where;
}

- (NSArray *) getFoodListLike:(NSString*)pattern
{
    return [self getFoodListLikeWithSettings:pattern];
    
    NSString *query;
    query = [NSString stringWithFormat:@" \
             SELECT food.id, food.name, foodGroup.id, foodGroup.name, oxalateGroup.id, oxalateGroup.name \
             FROM food \
             JOIN foodGroup ON food.foodGroup=foodGroup.id \
             JOIN oxalateGroup ON food.oxalateGroup=oxalateGroup.id \
             WHERE food.name LIKE '%%%@%%' \
             ORDER BY food.name; ", pattern ];
    
    return [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
}

- (NSArray *) getFoodListLikeWithSettings:(NSString *)pattern
{
    NSString *query = @"";
    NSString *where;
    NSString *temp;
    
    where = [NSString stringWithFormat:@" WHERE food.name LIKE '%%%@%%' ", pattern];
    temp = [self getQueryWhere];
    if ([temp length])
    {
        temp = [temp substringWithRange:NSMakeRange(6, [temp length]-6)];
        where = [NSString stringWithFormat:@"%@ AND ( %@ )",where, temp ];
    }
    
    query = [NSString stringWithFormat:@" \
             SELECT food.id, food.name, foodGroup.id, foodGroup.name, oxalateGroup.id, oxalateGroup.name \
             FROM food \
             JOIN foodGroup ON food.foodGroup=foodGroup.id \
             JOIN oxalateGroup ON food.oxalateGroup=oxalateGroup.id \
             %@ \
             ORDER BY food.name; ", where ];
    
//    NSLog(@"Where: %@", where);
//    NSLog(@"Query: %@", query);
    
    return [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
}


- (NSArray *) getFood:(NSInteger) foodId
{
    NSString *query;
    query = [ NSString stringWithFormat:@"\
             SELECT food.id, food.name, food.foodGroup, food.oxalateGroup, foodGroup.name, oxalateGroup.name \
             FROM food \
             JOIN foodGroup ON food.foodGroup=foodGroup.id \
             JOIN oxalateGroup ON food.oxalateGroup=oxalateGroup.id \
             WHERE food.id=%d ", foodId ];
    
    NSArray *result;
    result = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];

    return result;
}

- (NSArray *) getFoodGroup
{
    NSString *query;
    NSArray *result;

    query = @"SELECT id, name FROM foodGroup ORDER BY id ASC";
    result = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];

    return result;
}

- (NSArray *) getOxalateGroup
{
    NSString *query;
    query = @"SELECT * FROM oxalateGroup";

    NSArray *result;
    result = [[NSArray alloc] initWithArray:[self.dbManager loadDataFromDB:query]];
    
    return result;
}

- (NSInteger) updateFood:(NSString*) foodName
             foodId:(NSInteger)foodId
          foodGroup:(NSInteger)foodGroup
       oxalateGroup:(NSInteger)oxalateGroup
{
    NSString *query;
    query = [NSString stringWithFormat:@"UPDATE food \
        SET name='%@', foodGroup='%d', oxalateGroup='%d' \
        WHERE id=%d; ", foodName, foodGroup, oxalateGroup, foodId];

    
    // Execute the query.
    [self.dbManager executeQuery:query];
    
    // If the query was successfully executed then pop the view controller.
    if (self.dbManager.affectedRows != 0)
    {
        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
        return 0;
    }
    else
    {
        NSLog(@"Could not execute the query.");
        return -1;
    }
}

- (NSInteger) insertFood:(NSString*) foodName
               foodGroup:(NSInteger)foodGroup
            oxalateGroup:(NSInteger)oxalateGroup
{
    NSString *query;
    query = [NSString stringWithFormat:@" \
             INSERT INTO food \
             VALUES(null, '%@', '%d', '%d');" , foodName, foodGroup, oxalateGroup];

    // Execute the query.
    [self.dbManager executeQuery:query];
    
    // If the query was successfully executed then pop the view controller.
    if (self.dbManager.affectedRows != 0)
    {
        NSLog(@"Query was executed successfully. Affected rows = %d", self.dbManager.affectedRows);
        return 0;
    }
    else
    {
        NSLog(@"Could not execute the query.");
        return -1;
    }
}

- (void) deleteFood:(NSInteger)foodId
{
    NSString *query;
    query = [NSString stringWithFormat:@" \
             DELETE FROM food \
             WHERE id=%d;" , foodId];
    
    // Execute the query.
    [self.dbManager executeQuery:query];
}

@end
